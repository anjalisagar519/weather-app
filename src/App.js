import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
import axios from 'axios';

function App() {

  const [city,setCity] = useState('paris')
  const [temperature,setTemperature] =useState(0)
  const [loading,setLoading] =useState(false)

  
  
  function selectCity(cityName,latitude,longitude){
    setLoading(true)
    setCity(cityName)

  axios.get('https://api.open-meteo.com/v1/forecast?latitude='+latitude+'&longitude='+longitude+'&current_weather=true&hourly=temperature_2m,relativehumidity_2m,windspeed_10m')
     .then(data=>{
      const temperatureValue =data.data.current_weather.temperature
      setTemperature(temperatureValue)
      setLoading(false)
     })
     .catch(error=>{
      console.log(error)
     })
  
  }
    


  return (
    <div className="App">
      <img class="weather_img" src='https://lh3.googleusercontent.com/ZcuuJFjzq62BKHGK2YZaTsoqQ6Dvkrk35qXvMihmbk0cHsIp0OUgIZr0FfsoRHxHJE6QcBWQCQXzMl7e79fusG8yWiWq6XOyySuFCw=h200-rw' />
      <h1>My weather App</h1>
      <div>
        <button onClick={()=>{selectCity('Cochi',9.93,76.26)}}>Cochi</button>
        <button onClick={()=>{selectCity('Trivandrum',8.52,76.93)}}>Trivandrum</button>
        <button onClick={()=>{selectCity('Calicut',11.25,75.78)}}>Calicut</button>
      </div>
      {loading?<p>Loading...</p>:<p>The current temperature at <span id="city">{city}</span> &nbsp;is &nbsp;<span id="temp">{temperature}°C</span> </p>}
      
    </div>
  );
}

export default App;
